Config {
   font               = "xft:Terminus Re33 Nerd:size=15:antialias=true:hinting=true:lcdfilter=lcddefault:dpi=96:hintstyle=hintfull:rgba=rgb"
   , additionalFonts  = ["xft:Ubuntu Mono:size=9:antialise=true:hinting=true"]
   , bgColor          = "black"
   , fgColor          = "white"
   , position         = Top
   , textOffset       = -1
   , iconOffset       = -1
   , alpha            = 257
   , border           = NoBorder
   , borderWidth      = 1
   , borderColor      = "cyan"
   , iconRoot         = "/home/jeronimo/xbm_icons"
   , lowerOnStart     = True
   , hideOnStart      = False
   , allDesktops      = True
   , overrideRedirect = True
   , pickBroadest     = True
   , persistent       = True
   , sepChar          = "%"
   , alignSep         = "}{"
   , template         = "<action=`mygtkmenu .menurc` button=1><action=`Gmrun` button=3><icon=Arch_mono_16x16.xpm/></action></action> %UnsafeStdinReader%}<fc=white><action=`Cal` button=1><action=`WS` button=3><action=`WS1` button=2>%date%</action></action></action></fc>{<action=`networkmanager_dmenu` button=1><action=`speedtest` button=3>%wlp3s0wi%</action></action> <action=`Mem` button=1><action=`Sensors` button=2><action=`Cpu` button=3>%multicpu% %memory%</action></action></action> <action=`Pavucontrol` button=1><fc=green>Звук:</fc> %XVol%</action> %kbd% <action=`BAT` button=1>%battery%</action>"
   , commands         =
      [ Run MultiCpu                            [ "--template" , "<fc=green>Проц: </fc><fc=white></fc><total>%"
                                                , "--Low"      , "50"        -- units: %
                                                , "--High"     , "85"        -- units: %
                                                , "--low"      , "#d2d4dc"
                                                , "--normal"   , "#d2d4dc"
                                                , "--high"     , "#fd0537"
                                                ] 10

      --, Run CoreTemp                            [ "--template" , "<fc=green>Темп: </fc><fc=white></fc><core0>°C"
      --                                          , "--Low"      , "2"         -- units: °C
      --                                          , "--High"     , "80"        -- units: °C
      --                                          , "--low"      , "#ffffff"
      --                                          , "--normal"   , "#ffffff"
      --                                          , "--high"     , "#8F0005"
      --                                          ] 10

      , Run Memory                              [ "--template" ,"<fc=green>ОЗУ: </fc><fc=white></fc><usedratio>%"
                                                , "--Low"      , "20"        -- units: %
                                                , "--High"     , "90"        -- units: %
                                                , "--low"      , "#ffffff"
                                                , "--normal"   , "ffffff"
                                                , "--high"     , "#8F0005"
                                                ] 10

      , Run Battery                             [ "--template", "<fc=green><acstatus></fc> <left>%"
                                                , "--Low"      , "10"        -- units: %
                                                , "--High"     , "95"        -- units: %
                                                , "--low"      , "#8F0005"
                                                , "--high"     , "#ffffff"
                                                , "--"
                                                , "-O", "<fc=green><icon=ac10.xbm/></fc>"
                                                , "-H", "-20"
                                                , "-i", "<icon=ac10.xbm/>"
                                                , "-o", "<icon=batt10.xbm/>"
                                                ] 10


       , Run Wireless           "wlp3s0"        ["-t", "<fc=green>Сеть:</fc> <quality>"
                                                ] 10

       , Run Com                "XVol"          [] "" 10

       , Run Kbd                                [ ("ru" , "<fc=#FFFFFF>RU</fc>")
                                                , ("us" , "<fc=#8F0005>US</fc>")
                                                ]

       , Run UnsafeStdinReader

       , Run Date                                "%H:%M  %d/%m/%Y г." "date" 60
                       ]
}
