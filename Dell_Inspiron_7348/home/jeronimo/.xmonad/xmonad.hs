-- Core
import XMonad
import Control.Monad (liftM2, filterM)
import qualified XMonad.StackSet as W
import qualified Data.Map as M
import qualified Data.ByteString as B
import Data.Tree
import System.Exit
import Graphics.X11.Xlib
import Graphics.X11.ExtraTypes.XF86
import Graphics.X11.Xinerama
import System.IO (Handle, hPutStrLn)
import qualified System.IO
import System.IO
import Data.List
-- Prompts
import XMonad.Prompt.RunOrRaise
import XMonad.Prompt.Man
import XMonad.Prompt.Window
-- Actions
import XMonad.Actions.MouseGestures
import XMonad.Actions.UpdatePointer
import XMonad.Actions.GridSelect
import XMonad.Actions.CycleWS
import XMonad.Actions.PhysicalScreens
import XMonad.Actions.OnScreen
import qualified XMonad.Actions.DynamicWorkspaceOrder as DO
import XMonad.Actions.TreeSelect
-- Utils
import XMonad.Util.Run (spawnPipe)
import XMonad.Util.Loggers
import XMonad.Util.EZConfig
import XMonad.Util.Scratchpad
import XMonad.Util.NamedScratchpad
-- Hooks
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.UrgencyHook
import XMonad.Hooks.Place
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.SetWMName
import XMonad.Hooks.ManageHelpers
import XMonad.Hooks.WorkspaceHistory
import XMonad.Hooks.Minimize
-- Layouts
import XMonad.Layout.Minimize
import XMonad.Layout.NoBorders
import XMonad.Layout.ResizableTile
import XMonad.Layout.Tabbed
import XMonad.Layout.DragPane
import XMonad.Layout.LayoutCombinators hiding ((|||))
import XMonad.Layout.DecorationMadness
import XMonad.Layout.TabBarDecoration
import XMonad.Layout.IM
import XMonad.Layout.Grid
import XMonad.Layout.Spiral
import XMonad.Layout.Mosaic
import XMonad.Layout.LayoutHints
import XMonad.Layout.Circle
import XMonad.Layout.NoBorders
import Data.Ratio ((%))
import XMonad.Layout.ToggleLayouts
import XMonad.Layout.Spacing
import XMonad.Layout.Gaps
import XMonad.Layout.LayoutScreens
import XMonad.Layout.TwoPane
import XMonad.Layout.OneBig
import XMonad.Layout.CenteredMaster
import XMonad.Layout.Magnifier
import XMonad.Layout.TrackFloating
import XMonad.Layout.IndependentScreens
import qualified XMonad.Layout.ToggleLayouts as Tog

--InternetBrowser
myBrowser                     = "chromium"
browserClass                  = "Chromium"
terminalClass                 = "URxvt"
myShell                       = "bash"
--CORE
mydefaults = def {
        terminal              = "urxvtc"
        , normalBorderColor   = "#303030"
        , focusedBorderColor  = "cyan"
        , workspaces          = myWorkspaces
        , modMask             = mod4Mask
        , borderWidth         = 1
        , startupHook         = setWMName "LG3D" <+> onScr 1 W.greedyView "1:W"
        , layoutHook          = myLayoutHook
        , manageHook          = manageHook def <+> myManageHook <+> manageScratchPad <+> namedScratchpadManageHook mynameScratchpads <+> placeHook (smart (0.5,0.5))
        , handleEventHook     = fullscreenEventHook <+> docksEventHook <+> minimizeEventHook
        } `additionalKeys` myKeys

--OTHER
onScr :: ScreenId -> (WorkspaceId -> WindowSet -> WindowSet) -> WorkspaceId -> X ()
onScr n f i = screenWorkspace n >>= \sn -> windows (f i . maybe id W.view sn)
role = stringProperty "WM_WINDOW_ROLE"
encodeCChar = map fromIntegral . B.unpack

--COLOR NON-ACTUAL WINDOW
xmobarTitleColor = "green"

--COLOR ACTUAL WINDOW
xmobarCurrentWorkspaceColor = "green"

--SCRATCHPAD
scratchPad = scratchpadSpawnActionTerminal "urxvtc -name scratchpad"
mynameScratchpads = [ NS "mocp" "urxvtc -name mocp -e mocp" (appName =? "MOCP") (customFloating $ W.RationalRect 0.2 0.2 0.6 0.6)
                    , NS "htop" "Htop" (appName =? "Htop") (customFloating $ W.RationalRect 0.2 0.2 0.6 0.6)
                    , NS "lxappearance" "lxappearance" (appName =? "lxappearance") (customFloating $ W.RationalRect 0.2 0.2 0.6 0.6)
                    , NS "xfce4-appfinder" "xfce4-appfinder" (appName =? "xfce4-appfinder") (customFloating $ W.RationalRect 0.001 0.03 0.3 0.6)
                    , NS "arandr" "arandr" (appName =? "arandr") (customFloating $ W.RationalRect 0.2 0.2 0.6 0.6)
                    , NS "nano" "urxvtc -name nano -e nano" (appName =? "nano") (customFloating $ W.RationalRect 0.2 0.2 0.6 0.6)
                    , NS "qbittorrent" "qbittorrent" (appName =? "qbittorrent") (customFloating $ W.RationalRect 0.2 0.2 0.6 0.6)
                    , NS "crow" "Crow Translate" (appName =? "crow") (customFloating $ W.RationalRect 0.3 0.3 0.5 0.5)
                    , NS "RosaImageWriter" "RosaImageWriter" (appName =? "RosaImageWriter") (customFloating $ W.RationalRect 0.3 0.3 0.4 0.4)
                    , NS "Nmtui" "Nmtui" (appName =? "nmtui") (customFloating $ W.RationalRect 0.3 0.3 0.4 0.4)
                    , NS "pavucontrol" "pavucontrol" (appName =? "pavucontrol") (customFloating $ W.RationalRect  0.2 0.2 0.6 0.6)
                    , NS "galculator" "galculator" (appName =? "galculator") (customFloating $ W.RationalRect  0.35 0.35 0.3 0.3)
                    , NS "gmrun" "gmrun" (className =? "Gmrun") (customFloating $ W.RationalRect  0.012 0.001 0.26 0.085)
                    ]

manageScratchPad = scratchpadManageHook (W.RationalRect l t w h)
                    where
                    h = 0.333   -- terminal height
                    w = 1       -- terminal width
                    t = 1 - h   -- distance from top edge
                    l = 1 - w   -- distance from left edge

--TILING
myLayoutHook = spacingRaw True (Border 0 4 4 4) True (Border 4 4 4 4) True
               $ avoidStruts
               $ toggleLayouts (noBorders Full)
               $ smartBorders
--               $ minimize
               $ tiled ||| mosaic 2 [3,2] ||| Full ||| magnifier (Tall 1 0.3 0.5)
                    where
                    tiled   = Tall nmaster delta ratio
                    nmaster = 1
                    delta   = 0.01
                    ratio   = 0.5

--WORKSPACES
xmobarEscape = concatMap doubleLts
    where doubleLts '<' = "<<"
          doubleLts x = [x]

myWorkspaces :: [String]
myWorkspaces = clickable . (map xmobarEscape) $ ["1:W","2:F","3:O","4:V","5:P","6:S","7:G","8:R"]
    where
               clickable l = [ "<action=xdotool key super+" ++ show (n) ++ ">" ++ ws ++ "</action>" | (i,ws) <- zip [1..9] l, let n = i ]

--WINDOWS RULES
myManageHook :: ManageHook
myManageHook = composeAll . concat $
        [ [className =? c --> doShift (myWorkspaces !! 0) <+> viewShift (myWorkspaces !! 0)        | c <- myWeb]
        , [className =? c --> doShift (myWorkspaces !! 1) <+> viewShift (myWorkspaces !! 1)        | c <- myDev]
        , [className =? c --> doShift (myWorkspaces !! 2) <+> viewShift (myWorkspaces !! 2)        | c <- myOffice]
        , [className =? c --> doShift (myWorkspaces !! 3) <+> viewShift (myWorkspaces !! 3)        | c <- myVMs]
        , [className =? c --> doShift (myWorkspaces !! 4) <+> viewShift (myWorkspaces !! 4)        | c <- myMedia]
        , [className =? c --> doShift (myWorkspaces !! 5) <+> viewShift (myWorkspaces !! 5)        | c <- mySocial]
        , [className =? c --> doShift (myWorkspaces !! 6) <+> viewShift (myWorkspaces !! 6)        | c <- myGames]
        , [className =? c --> doShift (myWorkspaces !! 7) <+> viewShift (myWorkspaces !! 7)        | c <- myExt]
        , [className =? c --> doCenterFloat                                                        | c <- myFloatC]
        , [appName   =? a --> doCenterFloat                                                        | a <- myFloatA]
        , [title     =? t --> doCenterFloat                                                        | t <- myFloatT]
        , [role      =? r --> doCenterFloat                                                        | r <- myFloatR]
        , [manageDocks]
        , [isFullscreen   --> doFullFloat]
        , [isDialog       --> doCenterFloat]
        , [transience']
        ]
        where
        myWeb        = ["Chromium","Thunderbird"]
        myDev        = ["Pcmanfm"]
        myOffice     = ["QElectroTech","MyPaint","Xsane","libreoffice-startcenter","libreoffice-impress","libreoffice-writer", "libreoffice-calc","libreoffice-draw"]
        myVMs        = ["VirtualBox","VBoxSDL","Virt-manager"]
        myMedia      = ["mpv","Shotwell","Gpicview","Totem"]
        mySocial     = ["ViberPC","TelegramDesktop"]
        myGames      = ["Crossover"]
        myExt        = ["TeamViewer"]
--FLOATING
        myFloatC     = ["GParted","Bleachbit","Nm-connection-editor","System-config-printer.py"]
        myFloatA     = ["xarchiver","Update","teamviewer","engrampa"]
        myFloatT     = ["Software Update"]
        myFloatR     = ["task_dialog","messages","pop-up","^conversation$","About"]
        viewShift    = doF . liftM2 (.) W.greedyView W.shift

--KEYBINDINGS
myKeys = [
--Управление окнами клавиатурой
        --  ((mod4Mask,                    xK_KP_Subtract),                 withFocused minimizeWindow)
        --, ((mod4Mask,                    xK_KP_Add),                      sendMessage RestoreNextMinimizedWin)
          ((mod1Mask,                    xK_Right),                       nextWS)
        , ((mod1Mask,                    xK_Left),                        prevWS)
        , ((mod4Mask    .|. controlMask, xK_Left),                        DO.moveTo Prev HiddenNonEmptyWS)
        , ((0,                           xK_F12),                         scratchPad)
        , ((mod4Mask    .|. controlMask, xK_Right),                       DO.moveTo Next HiddenNonEmptyWS)
        , ((mod4Mask    .|. shiftMask,   xK_Right),                       shiftToNext >> nextWS)
        , ((mod4Mask    .|. shiftMask,   xK_Left),                        shiftToPrev >> prevWS)
        , ((mod4Mask,                    xK_Right),                       windows W.focusUp)
        , ((mod4Mask,                    xK_Left),                        windows W.focusDown)
        , ((mod4Mask,                    xK_Up),                          sendMessage Expand)
        , ((mod4Mask,                    xK_Down),                        sendMessage Shrink)
        , ((mod1Mask,                    xK_F4),                          kill)
        , ((mod4Mask,                    xK_b),                           sendMessage ToggleStruts)
        , ((mod1Mask    .|. shiftMask,   xK_space),                       layoutScreens 2 (TwoPane 0.5 0.5))
        , ((mod4Mask    .|. shiftMask,   xK_space),                       rescreen)
        , ((mod1Mask    .|. controlMask, xK_1),                           spawn "DualVGA.sh && xmonad --recompile && xmonad --restart && killall xmobar && xmobar")
        , ((mod1Mask    .|. controlMask, xK_0),                           spawn "OneNout.sh && xmonad --recompile && xmonad --restart && killall xmobar && xmobar")
--APPS
--GROUPPED
        , ((mod4Mask    .|. shiftMask,   xK_d),                           spawnSelected def ["GParted","Optimizer","bleachbit","SSD","Bleachbit"])
--GUIs
        , ((mod4Mask,                    xK_g),                           spawn "chromium")
        , ((mod4Mask,                    xK_m),                           spawn "youtube")
        , ((controlMask .|. mod1Mask,    xK_g),                           spawn "geany")
        , ((controlMask .|. mod1Mask,    xK_Return),                      spawn "Viber")
        , ((mod4Mask,                    xK_p),                           spawn "Shotwell")
        , ((mod4Mask,                    xK_v),                           spawn "virt-manager")
        , ((mod4Mask,                    xK_t),                           spawn "thunderbird")
        , ((mod4Mask,                    xK_n),                           spawn "pcmanfm")
        , ((mod4Mask,                    xK_l),                           spawn "libreoffice")
        , ((mod4Mask,                    xK_Print),                       spawn "Cupsd")
        , ((mod4Mask,                    xK_w),                           spawn "VBoxSDL --startvm WindowsXP")
        , ((mod4Mask,                    xK_s),                           spawn "simple-scan")
--TERMs
        , ((mod4Mask,                    xK_k),                           spawn "if (pgrep galculator >/dev/null); then kill $(pgrep galculator); else /usr/bin/galculator; fi")
        , ((0,                           xK_Print),                       spawn "scrot -e 'mv $f /home/jeronimo/Изображения/Screenshots/ 2>/dev/null'")
        , ((0           .|. shiftMask,   xK_Print),                       spawn "scrot -u -q 100 -e 'mv $f /home/jeronimo/Изображения/Screenshots/ 2>/dev/null'")
        , ((mod1Mask,                    xK_Print),                       spawn "scrot -s -q 100 -e 'mv $f /home/jeronimo/Изображения/Screenshots/ 2>/dev/null'")
        , ((mod4Mask    .|. shiftMask,   xK_Print),                       spawn "Screencast")
        , ((mod4Mask,                    xK_F2),                          spawn "Gmrun")
        , ((mod4Mask,                    xK_Return),                      spawn "Telegram")
        , ((0,                           xF86XK_AudioNext),               spawn "mocp -f")
        , ((0,                           xF86XK_AudioStop),               spawn "kill $(pidof mocp)")
        , ((0,                           xF86XK_AudioPrev),               spawn "mocp -r")
        , ((0,                           xF86XK_AudioPlay),               spawn "mocp -G")
        , ((controlMask .|. mod1Mask,    xK_m),                           spawn "urxvtc -name MOCP -e mocp")
        , ((controlMask .|. mod1Mask,    xK_Print),                       spawn "system-config-printer")
        , ((controlMask .|. mod1Mask,    xK_t),                           spawn "urxvtc")
        , ((controlMask .|. mod1Mask,    xK_n),                           spawn "urxvtc -name nano -e nano")
        , ((controlMask .|. mod1Mask,    xK_a),                           spawn "if (pgrep arandr >/dev/null); then kill $(pgrep arandr); else /usr/bin/arandr; fi")
        , ((controlMask .|. mod1Mask,    xK_h),                           spawn "Htop")
       -- , ((controlMask .|. mod1Mask,    xK_v),                           spawn "virtualbox")
--SYSTEM ACTIONS
        , ((mod4Mask    .|. shiftMask,   xK_r),                           spawn "xmonad --recompile && xmonad --restart && killall xmobar && xmobar")
        , ((0,                           xF86XK_WLAN),                    spawn "WIFI")
        , ((0,                    xF86XK_MonBrightnessUp),                spawn "xbacklight -inc 5")
        , ((0,                    xF86XK_MonBrightnessDown),              spawn "xbacklight -dec 5")
        , ((0,                    xF86XK_AudioRaiseVolume),               spawn "amixer -D pulse set Master 5%+ unmute")
        , ((0,                    xF86XK_AudioLowerVolume),               spawn "amixer -D pulse set Master 5%- unmute")
        , ((0,                           xF86XK_AudioMute),               spawn "amixer -D pulse set Master toggle")
        , ((mod4Mask,                    xK_r),                           spawn "systemctl reboot")
        , ((0,                           xF86XK_Sleep),                   spawn "systemctl suspend")
        , ((mod4Mask,                    xK_Escape),                      spawn "systemctl poweroff")
        ]

--MOUSE ACTIONS
myMouseBindings (XConfig {XMonad.modMask = modm}) = M.fromList $
--MOVING THE WINDOW
        [ ((modm, button1), (\w -> focus w >> mouseMoveWindow w
                                           >> windows W.shiftMaster))
--RAISING THE WINDOW
        , ((modm, button2), (\w -> focus w >> windows W.shiftMaster))
--RESIZING THE WINDOW
        , ((modm, button3), (\w -> focus w >> mouseResizeWindow w
                                           >> windows W.shiftMaster))
        ]

--XMOBAR
main = do
        xmproc <- spawnPipe "xmobar"
        xmonad $ ewmh $ mydefaults {
        logHook =  dynamicLogWithPP $ def {
        ppOutput = System.IO.hPutStrLn xmproc
        , ppTitle = ( \ str -> "")
        , ppCurrent = xmobarColor xmobarCurrentWorkspaceColor "" . wrap """"
        , ppSep = "  "
        , ppWsSep = "  "
        , ppLayout = (\ x -> case x of
           "Spacing Tall"                 -> "[<icon=circle.xbm/>]"
           "Spacing Full"                 -> "[<icon=monocle.xbm/>]"
           "Spacing Mosaic"               -> "[<icon=bstack2.xbm/>]"
           "Spacing Magnifier Tall"       -> "[<icon=corner_left.xbm/>]"
           _                                         -> x )
 }
}
